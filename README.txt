Opiumworks related info

INSTALLING THIS APP
---------------------

Simply clone the repository and setup your domain/webfolders to serve from drupal's root directory.
There is an sqlite db, already containing all the needed configuration.
The running/key URLs are:

* /entries 					=> this is where the RESTfull API can return lists of entries, without filtering
* * /entries?from=2015-09-07			=> this is where the RESTfull API can return lists of entries, filtered from a given date
* * /entries?to=2015-09-08			=> this is where the RESTfull API can return lists of entries, filtered up to a given date
* * /entries?from=2015-09-07&to=2015-09-08	=> this is where the RESTfull API can return lists of entries, filtered from a given date and up to another given date
* /fb/						=> this is the URL for the Entry Form for the FB Canvas, should run under an https URL

