(function ($) {
  Drupal.behaviors.recaptcha = {
    attach: function (context) {
      FB.init({      
	  appId: 403350403194335,
      frictionlessRequests: true,
      status: true,
      version: 'v2.4'
      });

      FB.Event.subscribe('auth.authResponseChange', onAuthResponseChange);
      FB.Event.subscribe('auth.statusChange', onStatusChange);    
	},  
    detach: function (context) {}
	
  };
 
 
function login(callback) {
  FB.login(callback);
  console.log(callback);
}
function loginCallback(response) {
  console.log('loginCallback',response);
  if(response.status != 'connected') {
    top.location.href = 'https://www.facebook.com/appcenter/opiumworksfbsp';
  }
}

function onStatusChange(response) {
  if( response.status != 'connected' ) {
    login(loginCallback);
  } else {
    console.log("show home");
  }
}
function onAuthResponseChange(response) {
  console.log('onAuthResponseChange', response);
}
  
}(jQuery));

